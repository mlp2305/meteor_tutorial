
PlayersList = new Mongo.Collection('players');
if(Meteor.isClient){
//helpers
Template.leaderboard.helpers({
    'player': function(){
	return PlayersList.find()
    },
    'selectedClass': function(){
        var currentlySelectedId = Session.get("selectedPlayer")
        if (currentlySelectedId== this._id) {
		return "selected"	
	}else {
		return ""
	}
    }
});
//events
Template.leaderboard.events({
    'click .player': function(){
	    playerId = this._id;
	    Session.set('selectedPlayer', playerId);
	    var selectedPlayer = Session.get('selectedPlayer');
	    console.log("You clicked player "+playerId);
     }
});
}
if(Meteor.isServer){
    console.log("Hello server");
}


